package week8.day2.acme;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class Acme {
	
	@Test
	public void acme() {
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://acme-test.uipath.com/account/login");
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		//Login
		driver.findElementById("email").sendKeys("vidya.evergreen@gmail.com");
		driver.findElementById("password").sendKeys("Acme@123");
		driver.findElementById("buttonLogin").click();
		
		//Mouseover
		WebElement ele = driver.findElementByXPath("(//div[@class='dropdown'])[5]");
		Actions act = new Actions(driver);
		act.moveToElement(ele).perform();
		
		driver.findElementByLinkText("Search for Vendor").click();
		
		//Vendor ID
		driver.findElementById("vendorTaxID").sendKeys("DE767565");
		driver.findElementById("buttonSearch").click();
		
		//Get vendor name
		WebElement vendor = driver.findElementByXPath("//table/tbody/tr[2]/td");
		System.out.println("vendor name is "+vendor.getText());
		System.out.println("Done");
		
	}


}
